package br.ufsc.ine5605.trabalho01.control;

import java.util.ArrayList;

import br.ufsc.ine5605.trabalho01.model.Cargo;
import br.ufsc.ine5605.trabalho01.model.Funcionario;
import br.ufsc.ine5605.trabalho01.model.ICadastraListaExclui;
import br.ufsc.ine5605.trabalho01.model.Veiculo;
import br.ufsc.ine5605.trabalho01.persistencia.FuncionarioDAO;
import br.ufsc.ine5605.trabalho01.view.TelaVeiculosFunc;
import br.ufsc.ine5605.trabalho01.view.TelaAddVeiculosFunc;
import br.ufsc.ine5605.trabalho01.view.TelaAlterarFuncionario;
import br.ufsc.ine5605.trabalho01.view.TelaCadastroFuncionario;
import br.ufsc.ine5605.trabalho01.view.TelaGeralFuncionario;
import br.ufsc.ine5605.trabalho01.view.TelaListarFuncionario;

public class ControleFuncionario implements ICadastraListaExclui {

	// private ArrayList<Funcionario> funcionarios;
	private FuncionarioDAO funcionarioDAO;
	// private TelaFuncionario telaFuncionario;
	private TelaCadastroFuncionario telaCadastroFunc;
	private TelaListarFuncionario telaListarFunc;
	private TelaGeralFuncionario telaGeralFunc;
	private TelaVeiculosFunc telaVeiculosFunc;
	private TelaAddVeiculosFunc telaAddVeiculosFunc;
	private TelaAlterarFuncionario telaAlteraFunc;
	private int geraMatricula;
	private static ControleFuncionario instance;

	public FuncionarioDAO getFuncionarioDAO() {
		if (funcionarioDAO == null) {
			return new FuncionarioDAO();
		}
		return funcionarioDAO;
	}

	// Singleton
	private ControleFuncionario() {
		// this.funcionarios = new ArrayList<>();
		funcionarioDAO = new FuncionarioDAO();
		// this.telaFuncionario = new TelaFuncionario(this);
		this.telaCadastroFunc = new TelaCadastroFuncionario(this);
		this.telaGeralFunc = new TelaGeralFuncionario(this);
		this.telaListarFunc = new TelaListarFuncionario(this);
		this.telaVeiculosFunc = new TelaVeiculosFunc(this);
		this.telaAddVeiculosFunc = new TelaAddVeiculosFunc(this);
		this.telaAlteraFunc = new TelaAlterarFuncionario(this);
	}

	public static synchronized ControleFuncionario getInstance() {
		if (instance == null) {
			instance = new ControleFuncionario();
		}
		return instance;
	}
	// Fim singleton

	@Override
	public void cadastra(Object func) throws Exception {
		Funcionario funcionario = (Funcionario) func;
		if (funcionario != null) {
			if (!funcionarioExiste(funcionario.getMatricula())) {
				this.funcionarioDAO.put(funcionario);
				telaListarFunc.updateData();
			} else {
				throw new Exception("Funcion�rio existente");
			}
		} else {
			throw new Exception("Funcion�rio nulo");
		}
	}

	public void recebeDadosTela(int matricula, String nome, String dataDeNascimento, String telefone, int opcaoCargo)
			throws Exception {
		if (!nome.equals("") && nome != null) {
			if (!dataDeNascimento.equals("") && dataDeNascimento != null) {
				if (!telefone.equals("") && telefone != null) {
					Funcionario funcionario = new Funcionario(matricula, nome, dataDeNascimento, telefone);
					cadastra(funcionario);
					cadastraCargo(opcaoCargo, funcionario);
					telaListarFunc.updateData();
				} else {
					throw new Exception("Digite o seu telefone para efetuar o cadastro.");
				}
			} else {
				throw new Exception("Digite a sua data de nascimento para efetuar o cadastro.");
			}
		} else {
			throw new Exception("Digite seu nome para efetuar o cadastro.");
		}
	}

	public void cadastraCargo(int opcaoCargo, Funcionario funcionario) throws Exception {
		switch (opcaoCargo) {
		case 1:
			funcionario.setCargo(Cargo.DIRETORIA);
			adicionaVeiculosDiretor(funcionario.getMatricula());
			this.funcionarioDAO.persist();
			break;
		case 2:
			funcionario.setCargo(Cargo.SERVICOSGERAIS);
			telaAddVeiculos(funcionario.getMatricula());
			this.funcionarioDAO.persist();
			break;
		case 3:
			funcionario.setCargo(Cargo.RH);
			telaAddVeiculos(funcionario.getMatricula());
			this.funcionarioDAO.persist();
			break;
		case 4:
			funcionario.setCargo(Cargo.INFORMATICA);
			telaAddVeiculos(funcionario.getMatricula());
			this.funcionarioDAO.persist();
			break;
		default:
			break;
		}
	}

	public void adicionaVeiculoAoFuncionario(String placa, int matricula) throws Exception {
		if (Claviculario.getInstance().getCtrlVeiculo().existeVeiculoPlaca(placa)) {
			if (funcionarioExiste(matricula)) {
				if (!funcionarioTemVeiculo(matricula, placa)) {
					encontraFuncionarioPorMatricula(matricula).getVeiculosPermitidos()
							.add(Claviculario.getInstance().getCtrlVeiculo().encontraVeiculoPorPlaca(placa));
					funcionarioDAO.persist();
				}else{
					throw new Exception("Funcionario j� tem este veiculo");
				}
				// telaAddVeiculosFunc.updateDataFuncVeiculos();
				// telaAddVeiculosFunc.updateDataVeiculos();
			}
		}
	}

	public void removeVeiculo(String placa, Funcionario funcionario) throws Exception {
		if (!Claviculario.getInstance().getCtrlEmprestimo().verificaEmprestimoMatriculaPlaca(funcionario.getMatricula(),
				placa)) {
			if (Claviculario.getInstance().getCtrlVeiculo().existeVeiculoPlaca(placa)) {
				if (funcionario != null) {
					if (funcionarioExiste(funcionario.getMatricula())) {
						funcionario.getVeiculosPermitidos()
								.remove(Claviculario.getInstance().getCtrlVeiculo().encontraVeiculoPorPlaca(placa));
						funcionarioDAO.persist();
					}
				}
			}
		} else {
			throw new Exception(
					"Veiculo est� alugado fa�a a devolu��o para poder retirar de sua lista de veiculos permitidos");
		}
	}

	public void removeVeiculosFuncionarios(String placa) throws Exception {
		if (!funcionarioDAO.getList().isEmpty()) {
			if (Claviculario.getInstance().getCtrlVeiculo().isPlacaValida(placa)) {
				for (Funcionario f : funcionarioDAO.getList()) {
					Veiculo vExcluir = null;
					for (Veiculo v : f.getVeiculosPermitidos()) {
						if (v.getPlaca().equals(placa)) {
							vExcluir = v;
						}
					}
					if (vExcluir != null) {
						f.getVeiculosPermitidos().remove(vExcluir);
					}
				}
			}
		}
	}

	public Veiculo encontraVeiculoNoFuncionario(String placa) {
		for (Funcionario f : funcionarioDAO.getList()) {
			for (Veiculo v : f.getVeiculosPermitidos()) {
				if (v.getPlaca().equals(placa)) {
					return v;
				}
			}
		}
		return null;
	}

	public boolean funcionarioTemVeiculo(int matricula, String placa) {
		for (Funcionario f : funcionarioDAO.getList()) {
			if (f.getMatricula() == matricula) {
				for (Veiculo v : f.getVeiculosPermitidos()) {
					if (v.getPlaca().equals(placa)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void alteraTela(int matricula, String nome, String dataDeNascimento, String telefone, int opcaoCargo)
			throws Exception {
		Funcionario funcionario = encontraFuncionarioPorMatricula(matricula);
		if (!funcionarioDAO.getList().isEmpty()) {
			if (funcionario != null) {
				funcionario.setNome(nome);
				funcionario.setDataDeNascimento(dataDeNascimento);
				funcionario.setTelefone(telefone);
				switch (opcaoCargo) {
				case 1:
					funcionario.setCargo(Cargo.DIRETORIA);
					adicionaVeiculosDiretor(funcionario.getMatricula());
					funcionario.setMatricula(atualizaMatricula(matricula, opcaoCargo));
					this.funcionarioDAO.put(funcionario);
					funcionarioDAO.remove(matricula);
					break;
				case 2:
					funcionario.setCargo(Cargo.SERVICOSGERAIS);
					funcionario.setMatricula(atualizaMatricula(matricula, opcaoCargo));
					this.funcionarioDAO.put(funcionario);
					funcionarioDAO.remove(matricula);
					break;
				case 3:
					funcionario.setCargo(Cargo.RH);
					funcionario.setMatricula(atualizaMatricula(matricula, opcaoCargo));
					this.funcionarioDAO.put(funcionario);
					funcionarioDAO.remove(matricula);
					break;
				case 4:
					funcionario.setCargo(Cargo.INFORMATICA);
					funcionario.setMatricula(atualizaMatricula(matricula, opcaoCargo));
					this.funcionarioDAO.put(funcionario);
					funcionarioDAO.remove(matricula);
					break;
				default:
					break;
				}
				// switch (opcaoCargo) {
				// case 1:
				// cadastraCargo(opcaoCargo, funcionario);
				// case 2:
				// funcionario.setCargo(Cargo.SERVICOSGERAIS);
				// case 3:
				// funcionario.setCargo(Cargo.RH);
				// case 4:
				// funcionario.setCargo(Cargo.INFORMATICA);
				// }
			}
		}
	}

	public int atualizaMatricula(int matricula, int opcaoCargo) {
		switch (opcaoCargo) {
		case 1:
			Integer mat = matricula;
			String sMatricula = "1" + Integer.toString(matricula).substring(1, mat.toString().length());
			return Integer.parseInt(sMatricula);
		case 2:
			mat = matricula;
			sMatricula = "2" + Integer.toString(matricula).substring(1, mat.toString().length());
			return Integer.parseInt(sMatricula);
		case 3:
			mat = matricula;
			sMatricula = "3" + Integer.toString(matricula).substring(1, mat.toString().length());
			return Integer.parseInt(sMatricula);
		case 4:
			mat = matricula;
			sMatricula = "4" + Integer.toString(matricula).substring(1, mat.toString().length());
			return Integer.parseInt(sMatricula);
		}
		return -1;
	}

	@Override
	public String lista() throws Exception {
		String lista = "";
		int n = 1;
		if (!this.funcionarioDAO.getList().isEmpty()) {
			for (Funcionario f : this.funcionarioDAO.getList()) {
				lista += "========================\n";
				lista += "Matricula: " + f.getMatricula() + "\n";
				lista += "Nome: " + f.getNome() + "\n";
				lista += "Cargo: " + f.getCargo() + "\n";
				lista += "Data de nascimento: " + f.getDataDeNascimento() + "\n";
				lista += "Telefone: " + f.getTelefone() + "\n";
				lista += "-------Carros permitidos-------\n";
				// temporario
				for (Veiculo v : f.getVeiculosPermitidos()) {
					lista += "--------------" + n + "--------------\n";
					lista += "Placa: " + v.getPlaca() + "\n";
					lista += "Modelo: " + v.getModelo() + "\n";
					lista += "Marca: " + v.getMarca() + "\n";
					lista += "Ano: " + v.getAno() + "\n";
					lista += "Quilometragem: " + v.getQuilometragem() + "\n";
					// lista += "Disponibilidade: " + v.isDisponibilidade() +
					// "\n";
					n++;
				}
				lista += "========================\n";
			}
		} else {
			lista += "========================\n";
			lista += "Lista vazia\n";
			lista += "========================\n";
		}
		return lista;
	}

	public String exibeFuncionario(int matricula) {
		String lista = "";
		int n = 1;
		if (funcionarioExiste(matricula)) {
			for (Funcionario f : this.funcionarioDAO.getList()) {
				if (f.getMatricula() == matricula) {
					lista += "========================\n";
					lista += "Matricula: " + f.getMatricula() + "\n";
					lista += "Nome: " + f.getNome() + "\n";
					lista += "Cargo: " + f.getCargo() + "\n";
					lista += "Data de nascimento: " + f.getDataDeNascimento() + "\n";
					lista += "Telefone: " + f.getTelefone() + "\n";
					lista += "-------Carros permitidos-------\n";
					// temporario
					for (Veiculo v : f.getVeiculosPermitidos()) {
						lista += "--------------" + n + "--------------\n";
						lista += "Placa: " + v.getPlaca() + "\n";
						lista += "Modelo: " + v.getModelo() + "\n";
						lista += "Marca: " + v.getMarca() + "\n";
						lista += "Ano: " + v.getAno() + "\n";
						lista += "Quilometragem: " + v.getQuilometragem() + "\n";
						lista += "Disponibilidade: " + v.isDisponibilidade() + "\n";
						n++;
					}
					lista += "========================\n";
				}
			}
		} else {
			lista += "===========================\n";
			lista += "Funcionario n�o encontrado\n";
			lista += "===========================\n";
		}
		return lista;
	}

	public ArrayList<Veiculo> getVeiculosFuncPorMatricula(int matricula) {
		for (Funcionario f : this.funcionarioDAO.getList()) {
			if (f.getMatricula() == matricula) {
				return f.getVeiculosPermitidos();
			}
		}
		return null;
	}

	// Inativo
	// public ArrayList<Funcionario> getFuncionarios() {
	// return new ArrayList(funcionarioDAO.getList());
	// }

	// Inativo
	@Override
	public void exclui(Object func) throws Exception {
		/*
		 * Funcionario funcionario = (Funcionario) func; if (funcionario !=
		 * null) { if (!this.funcionarios.isEmpty()) {
		 * this.funcionarios.remove(funcionario); } } else { throw new
		 * Exception("Funcion�rio n�o existe"); }
		 */
	}

	public void removeFuncionarioPorMatricula(int matricula) throws Exception {
		if (!Claviculario.getInstance().getCtrlEmprestimo().verificaTemEmprestimoFuncionario(matricula)) {
			if (funcionarioExiste(matricula)) {
				/*
				 * Funcionario f = encontraFuncionarioPorMatricula(matricula);
				 * if (f != null) { exclui(f); throw new Exception(
				 * "Funcionario: "+matricula+", foi excluido"); }
				 */
				funcionarioDAO.remove(new Integer(matricula));
				telaListarFunc.updateData();
			}
		} else {
			throw new Exception("Funcionario: " + encontraFuncionarioPorMatricula(matricula).getNome()
					+ " tem emprestimo, fa�a a devolu��o antes de apagar");
		}
	}

	public Veiculo carroAlugado(int matricula) {
		if (Claviculario.getInstance().getCtrlEmprestimo().verificaTemEmprestimoFuncionario(matricula)) {
			Veiculo v = Claviculario.getInstance().getCtrlEmprestimo().encontraVeiculoPorMatricula(matricula);
			return v;
		}
		return null;
	}

	public boolean funcionarioExiste(int matricula) {
		/*
		 * if (!this.funcionarios.isEmpty()) { for (Funcionario f :
		 * this.funcionarios) { if (matricula != 0 && f.getMatricula() ==
		 * matricula) { return true; } } }
		 */
		if (encontraFuncionarioPorMatricula(matricula) != null) {
			return true;
		}
		return false;
	}

	public Funcionario encontraFuncionarioPorMatricula(int matricula) {
		/*
		 * if (!this.funcionarios.isEmpty()) { for (Funcionario f :
		 * this.funcionarios) { if (f.getMatricula() == matricula) { return f; }
		 * } }
		 */
		return funcionarioDAO.get(new Integer(matricula));
		// throw new Exception("Funcionario n�o encontrado");
	}

	public boolean isDiretor(int matricula) throws Exception {
		if (encontraFuncionarioPorMatricula(matricula).getCargo().equals(Cargo.DIRETORIA)) {

			return true;
		}
		return false;
	}

	public void adicionaVeiculosTodosDiretores() {
		if (!Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList().isEmpty()) {
			for (Funcionario f : this.funcionarioDAO.getList()) {
				if (f.getCargo() == Cargo.DIRETORIA) {
					if (f.getVeiculosPermitidos().size() != 0) {
						f.getVeiculosPermitidos().removeAll(f.getVeiculosPermitidos());
						f.getVeiculosPermitidos()
								.addAll(Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList());
						funcionarioDAO.persist();
					} else {
						f.getVeiculosPermitidos()
								.addAll(Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList());
						funcionarioDAO.persist();
					}
				}
			}
		}
	}

	public void adicionaVeiculosDiretor(int matricula) throws Exception {
		if (!Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList().isEmpty()) {
			if (encontraFuncionarioPorMatricula(matricula).getVeiculosPermitidos().size() != 0) {
				encontraFuncionarioPorMatricula(matricula).getVeiculosPermitidos()
						.removeAll(Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList());
				funcionarioDAO.persist();
			}
			encontraFuncionarioPorMatricula(matricula).getVeiculosPermitidos()
					.addAll(Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList());
			funcionarioDAO.persist();
		}
	}

	public int geraMatricula(int opcaoCargo) {
		int mat = opcaoCargo;
		String sMatricula;
		do {
			geraMatricula++;
			sMatricula = Integer.toString(opcaoCargo) + geraMatricula;
			mat = Integer.parseInt(sMatricula);
		} while (funcionarioExiste(mat));
		return mat;
	}

	public boolean isDataNascValida(String dataDeNasc) {
		boolean validade = false;

		if (dataDeNasc.matches("\\d{2}/\\d{2}/\\d{2}") && !dataDeNasc.substring(0, 1).equals("00")
				&& !dataDeNasc.substring(3, 4).equals("00")
				&& !dataDeNasc.substring(6, dataDeNasc.length()).equals("00")) {
			validade = true;
		}
		return validade;
	}

	// METODOS TELA
	public void inicia() {
		// telaFuncionario.exibeMenu();
		telaGeralFunc.setVisible(true);
	}

	public void telaCadastroFunc() {
		telaCadastroFunc.setVisible(true);
	}

	public void telaListarFunc() {
		telaListarFunc.init();
		adicionaVeiculosTodosDiretores();
		telaListarFunc.setVisible(true);
	}

	public void telaVeiculosFunc(int matricula) {
		if (matricula != 0) {
			telaVeiculosFunc.pegaMatricula(matricula);
			telaVeiculosFunc.init();
			telaVeiculosFunc.updateData();
			telaVeiculosFunc.setVisible(true);
		}
	}

	public void atualizaTabelaVeiculosFunc() {
		telaVeiculosFunc.updateData();
	}

	public void telaAddVeiculos(int matricula) {
		telaAddVeiculosFunc.init();
		telaAddVeiculosFunc.pegaMatricula(matricula);
		telaAddVeiculosFunc.updateDataVeiculos();
		telaAddVeiculosFunc.updateDataFuncVeiculos();
		telaAddVeiculosFunc.setVisible(true);

	}

	public void telaAlterarFunc(int matricula) {
		if (matricula != 0) {
			telaAlteraFunc.pegaMatricula(matricula);
			telaAlteraFunc.preencheCampos();
			telaAlteraFunc.setVisible(true);
		}
	}
	// public void atualizaTabelas(){
	// telaVeiculosFunc.updateData();
	// telaListarFunc.updateData();
	// telaAddVeiculosFunc.updateDataVeiculos();
	// }
}
