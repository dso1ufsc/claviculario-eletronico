package br.ufsc.ine5605.trabalho01.control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.ufsc.ine5605.trabalho01.model.Funcionario;
import br.ufsc.ine5605.trabalho01.model.Logs;
import br.ufsc.ine5605.trabalho01.model.Veiculo;
import br.ufsc.ine5605.trabalho01.persistencia.LogsDAO;
import br.ufsc.ine5605.trabalho01.view.TelaLogs;

public class ControleLogs {
	private LogsDAO logsDAO;
	private int cod;
	private TelaLogs telaLogs;

	private static ControleLogs instance;
	
	public static synchronized ControleLogs getInstance(){
		if(instance == null){
			instance = new ControleLogs();
		}
		return instance;
	}
	
	public ControleLogs() {
		this.logsDAO = new LogsDAO();
		this.telaLogs = new TelaLogs(this);
		this.cod = 1;
	}

	public LogsDAO getLogsDAO() {
		if (logsDAO == null) {
			return new LogsDAO();
		}
		return logsDAO;
	}
	
	public boolean existeLog(int codigo){
		if(encontraLogPorCodigo(codigo)!=null){
			return true;
		}
		return false;
	}
	public Logs encontraLogPorCodigo(int codigo){
		return logsDAO.get(new Integer(codigo));
	}
	public ArrayList<Logs> encontraLogPorMotivo(String motivo){
		ArrayList<Logs> logsMotivo = new ArrayList<>();
		logsMotivo = new ArrayList<>();
		for(Logs log : this.getLogsDAO().getList()){
			if(log.getStatus().equals(motivo)){
				logsMotivo.add(log);
			}
		}
		return logsMotivo;
	}
	public ArrayList<Logs> encontraLogPorMatriculaFuncionario(int matricula){
		ArrayList<Logs> logsMatricula = new ArrayList<>();
		for(Logs log : this.getLogsDAO().getList()){
			if(log.getFuncionario().getMatricula() == matricula){
				logsMatricula.add(log);
			}
		}
		return logsMatricula;
	}
	public ArrayList<Logs> encontraLogPorPlacaVeiculo(String placa){
		ArrayList<Logs> logsPlaca = new ArrayList<>();
		logsPlaca = new ArrayList<>();
		for(Logs log : this.getLogsDAO().getList()){
			if(log.getVeiculo().getPlaca().equals(placa)){
				logsPlaca.add(log);
			}
		}
		return logsPlaca;
	}
	public ArrayList<Logs> getLogs(){
		ArrayList<Logs> logsAll = new ArrayList<>();
		logsAll = new ArrayList<>();
		for(Logs log : this.getLogsDAO().getList()){
			logsAll.add(log);
		}
		return logsAll;
	}
	public int geraCodigo(){
		do{
			cod++;
		}while(existeLog(cod));
		return cod;
	}
	public void geraLog(int acesso, Funcionario funcionario, Veiculo veiculo) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		Logs logs = null;
		cod = geraCodigo();
		if(acesso == 1){
			logs = new Logs(cod, sdf.format(new Date()), "Permitido", funcionario, veiculo);
		}else if(acesso == 2){
			logs = new Logs(cod, sdf.format(new Date()), "Negado", funcionario, veiculo);
		}else if(acesso == 3){
			logs = new Logs(cod, sdf.format(new Date()), "Devolvido", funcionario, veiculo);
		}
		try {
			logsDAO.put(logs);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
//METODOS TELA
	public void inicia() {
		telaLogs.init();
		telaLogs.updateData(getLogs());
		telaLogs.setVisible(true);
	}
	
}
