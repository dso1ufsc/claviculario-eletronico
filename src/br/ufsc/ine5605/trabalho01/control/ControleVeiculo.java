package br.ufsc.ine5605.trabalho01.control;

import java.util.ArrayList;

import br.ufsc.ine5605.trabalho01.model.ICadastraListaExclui;
import br.ufsc.ine5605.trabalho01.model.Veiculo;
import br.ufsc.ine5605.trabalho01.view.TelaAlterarVeiculo;
import br.ufsc.ine5605.trabalho01.view.TelaCadastroVeiculo;
import br.ufsc.ine5605.trabalho01.view.TelaControleVeiculo;
import br.ufsc.ine5605.trabalho01.persistencia.VeiculoDAO;

public class ControleVeiculo implements ICadastraListaExclui {

	//private TelaVeiculo telaVeiculo;
	private TelaCadastroVeiculo telaCadastroVeiculo;
	private static ControleVeiculo instance;
	private VeiculoDAO veiculoDAO;
	private TelaControleVeiculo telaControleVeiculo;
	private TelaAlterarVeiculo telaAlterarVeiculo;
	
	public VeiculoDAO getVeiculoDAO() {
		if (veiculoDAO == null) {
			return new VeiculoDAO();
		}
		return veiculoDAO;
	}

	public void setVeiculoDAO(VeiculoDAO veiculoDAO) {
		this.veiculoDAO = veiculoDAO;
	}

	// Singleton
	private ControleVeiculo() {
		//this.telaVeiculo = new TelaVeiculo(this);
		this.telaCadastroVeiculo = new TelaCadastroVeiculo(this);
		this.veiculoDAO = new VeiculoDAO();
	}

	public static synchronized ControleVeiculo getInstance() {
		if (instance == null) {
			instance = new ControleVeiculo();
		}
		return instance;
	}
	// Fim singleton

	// Cadastra, exclui, altera, lista
	@Override
	public void cadastra(Object vcl) throws Exception {
		Veiculo veiculo = (Veiculo) vcl;
		if (veiculo != null) {
			if (!existeVeiculoPlaca(veiculo.getPlaca())) {
				this.veiculoDAO.put(veiculo);
			} else {
				throw new Exception("Veiculo " +veiculo.getPlaca() +" ja existe");
			}
		} else {
			throw new Exception("Veiculo nulo");
		}
	}

	public void recebeDadosCadastroTela(String placa, String modelo, String marca, int ano, int quilometragem) throws Exception {
		if (isPlacaValida(placa)) {
			if (isModeloAndMarcaValido(modelo)) {
				if (isModeloAndMarcaValido(marca)) {
					String sAno = Integer.toString(ano);
					if (isAnoValido(sAno)) {
						String sQuilometragem = Integer.toString(quilometragem);
						if (isQuilometragemValido(sQuilometragem)) {
							Veiculo v = new Veiculo(placa, modelo, marca, ano, quilometragem);
							//cadastra(v);
							telaControleVeiculo.init();
							veiculoDAO.put(v);
						} else {
							throw new Exception("Digite uma quilometragem valida");
						}
					} else {
						throw new Exception("Digite um ano valido. O ano deve ser superior a 1900.");
					}
				} else {
					throw new Exception("Digite uma marca valida");
				}
			} else {
				throw new Exception("Digite um modelo valido");
			}
		} else {
			throw new Exception("Digite uma placa valida");
		}
	}

	@Override
	public void exclui(Object vcl) throws Exception {
		Veiculo veiculo = (Veiculo) vcl;
		if (veiculo != null) {
			this.veiculoDAO.remove(veiculo.getPlaca());
		} else {
			throw new Exception("Placa inv�lida");
		}
	}

	public void altera(String placa, String novaPlaca, String modelo, String marca, int ano, int quilometragem) throws Exception {
		Veiculo v = encontraVeiculoPorPlaca(placa);
		v.setPlaca(novaPlaca);
		v.setModelo(modelo);
		v.setMarca(marca);
		v.setAno(ano);
		v.setQuilometragem(quilometragem);
	}

	@Override
	public String lista() throws Exception {
		String lista = "";
		if (this.veiculoDAO.getList().size() != 0) {
			for (Veiculo v : this.veiculoDAO.getList()) {
				lista += "-------------------------------\n";
				lista += "Placa: " + v.getPlaca() + "\n";
				lista += "Modelo: " + v.getModelo() + "\n";
				lista += "Marca: " + v.getMarca() + "\n";
				lista += "Ano: " + v.getAno() + "\n";
				lista += "Quilometragem: " + v.getQuilometragem() + "\n";
				lista += "Disponibilidade: " + v.isDisponibilidade() + "\n";
				lista += "-------------------------------\n";
			}
		} else {
			throw new Exception("Lista vazia");
		}
		return lista;
	}

	public String pesquisaVeiculoPorPlaca(String placa) {
		String veiculo = "N�o encontrado";
		for (Veiculo v : this.veiculoDAO.getList()) {
			if (v.getPlaca().equals(placa) && isPlacaValida(placa)) {
				veiculo = "";
				veiculo += "-------------------------------\n";
				veiculo += "Placa: " + v.getPlaca() + "\n";
				veiculo += "Modelo: " + v.getModelo() + "\n";
				veiculo += "Marca: " + v.getMarca() + "\n";
				veiculo += "Ano: " + v.getAno() + "\n";
				veiculo += "Quilometragem: " + v.getQuilometragem() + "\n";
				veiculo += "Disponibilidade: " + v.isDisponibilidade() + "\n";
				veiculo += "-------------------------------\n";
				return veiculo;
			}
		}
		return veiculo;
	}
	
	public String pesquisaVeiculoPorMarca(String marca) {
		String veiculo = "N�o encontrado";
		for (Veiculo v : this.veiculoDAO.getList()) {
			if (v.getMarca().equals(marca)) {
				veiculo = "";
				veiculo += "-------------------------------\n";
				veiculo += "Placa: " + v.getPlaca() + "\n";
				veiculo += "Modelo: " + v.getModelo() + "\n";
				veiculo += "Marca: " + v.getMarca() + "\n";
				veiculo += "Ano: " + v.getAno() + "\n";
				veiculo += "Quilometragem: " + v.getQuilometragem() + "\n";
				veiculo += "Disponibilidade: " + v.isDisponibilidade() + "\n";
				veiculo += "-------------------------------\n";
				return veiculo;
			}
		}
		return veiculo;
	}

	public void removerVeiculo(String placa) throws Exception {
		if(!Claviculario.getInstance().getCtrlEmprestimo().verificaTemEmprestimoVeiculo(placa)){
			Claviculario.getInstance().getCtrlFuncionario().removeVeiculosFuncionarios(placa);
			exclui(encontraVeiculoPorPlaca(placa));
			telaControleVeiculo.updateData();
		}else{
			throw new Exception("Veiculo: "+placa+" est� alugado, devolva antes de excluir");
		}
		
	}

	public boolean existeVeiculoPlaca(String placa) {
		for (Veiculo v : this.veiculoDAO.getList()) {
			if (v.getPlaca().equals(placa)) {
				return true;
			}
		}
		return false;
	}

	public Veiculo encontraVeiculoPorPlaca(String placa) throws Exception {
		for (Veiculo v : this.veiculoDAO.getList()) {
			if (v.getPlaca().equals(placa)) {
				return v;
			}
		}
		throw new Exception("Ve�culo inexistente.");

	}
	public boolean isDisponivel(String placa) throws Exception{
		if(existeVeiculoPlaca(placa)){
			if(encontraVeiculoPorPlaca(placa).isDisponibilidade()){
				return true;
			}
		}
		return false;
	}
	
	public boolean alteraQuilometragem(int kmAtual, String placa) throws Exception{
		if(kmAtual > encontraVeiculoPorPlaca(placa).getQuilometragem()){
			encontraVeiculoPorPlaca(placa).setQuilometragem(kmAtual);
			return true;
		}else{
			throw new Exception("Quilometragem incorreta ou menor que a anterior");
		}
	}
	
	public void geraTabelaVeiculos() {
		
	}
	// ##########################################################################
	// ############################### Valida��es
	// ###############################
	// ##########################################################################

	public boolean isPlacaValida(String placa) {
		boolean validade = false;
		if (placa.length() == 7) {
			String numeroPlaca = placa.substring(3, 7);
			for (int i = 0; i < 3; i++) {
				if (Character.isLetter(placa.charAt(i)) && !numeroPlaca.equals("0000")) {
					validade = true;
				} else {
					return false;
				}
			}
			for (int i = 3; i < 7; i++) {
				if (Character.isDigit(placa.charAt(i))) {
					validade = true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
		return validade;
	}

	public boolean isModeloAndMarcaValido(String m) {
		boolean validade = false;
		for (int i = 0; i < m.length(); i++) {
			if (Character.isLetter(m.charAt(i)) || Character.isDigit(m.charAt(i))
					|| Character.isWhitespace(m.charAt(i))) {
				validade = true;
			} else {
				return false;
			}
		}
		return validade;
	}

	public boolean isAnoValido(String ano) {
		boolean validade = false;
		for (int i = 0; i < ano.length(); i++) {
			if (Character.isDigit(ano.charAt(i))) {
				if (Integer.valueOf(Integer.parseInt(ano)) > 1900) {
					validade = true;
				}
			} else {
				return false;
			}
		}
		return validade;
	}

	public boolean isQuilometragemValido(String quilometragem) {
		boolean validade = false;
		for (int i = 0; i < quilometragem.length(); i++) {
			if (Character.isDigit(quilometragem.charAt(i))) {
				validade = true;
			} else {
				return false;
			}
		}
		return validade;
	}

	// M�todos da tela
	public void abreCadastroVeiculo() {
		telaCadastroVeiculo.setVisible(true);
	}

	public void abreControleVeiculo() {
		this.telaControleVeiculo = new TelaControleVeiculo(this);
		telaControleVeiculo.setVisible(true);
	}

	public ArrayList<Veiculo> getVeiculos() {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateData() {
		// TODO Auto-generated method stub
		
	}

	public void abreTelaAlteraVeiculo(String placa2) {
		// TODO Auto-generated method stub
		this.telaAlterarVeiculo = new TelaAlterarVeiculo(this);
		telaAlterarVeiculo.pegaPlaca(placa2);
		telaAlterarVeiculo.init();
		telaAlterarVeiculo.setVisible(true);
		
	}

}
