package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.sun.javafx.scene.control.ControlAcceleratorSupport;

import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;
import br.ufsc.ine5605.trabalho01.model.Cargo;

public class TelaAlterarFuncionario extends JFrame{
	private ControleFuncionario ctrlFuncionario;
	private GerenciadorBotoesAlterarFunc gBtn;
	//componetes
	private int mat;
	private JLabel lbNome;
	private JTextField tfNome;
	private JLabel lbDataDeNascimento;
	private JTextField tfDataDeNascimento;
	private JLabel lbTelefone;
	private JTextField tfTelefone;
	private JComboBox<Cargo> cbCargo;
	private String[] cargos = { Cargo.DIRETORIA.toString(), Cargo.SERVICOSGERAIS.toString(), Cargo.RH.toString(),
			Cargo.INFORMATICA.toString() };
	private JButton btAlterar;
	
	public TelaAlterarFuncionario(ControleFuncionario owner){
		super("Alterar funcionario");
		this.ctrlFuncionario = owner;
		this.mat = 0;
		this.gBtn = new GerenciadorBotoesAlterarFunc();
		init();
		
	}
	public void init(){
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// NOME
		lbNome = new JLabel();
		lbNome.setText("Nome");
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(lbNome, constraints);
		tfNome = new JTextField();
		tfNome.setText("");
		constraints.gridx = 1;
		constraints.gridy = 0;
		tfNome.setPreferredSize(new Dimension(100, 20));
		container.add(tfNome, constraints);
		// DATAdeNASC
		lbDataDeNascimento = new JLabel();
		lbDataDeNascimento.setText("Data de nascimento");
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(lbDataDeNascimento, constraints);
		tfDataDeNascimento = new JTextField();
		tfDataDeNascimento.setText("");
		constraints.gridx = 1;
		constraints.gridy = 1;
		tfDataDeNascimento.setPreferredSize(new Dimension(100, 20));
		container.add(tfDataDeNascimento, constraints);
		// TELEFONE
		lbTelefone = new JLabel();
		lbTelefone.setText("Telefone");
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(lbTelefone, constraints);
		tfTelefone = new JTextField();
		tfTelefone.setText("");
		constraints.gridx = 1;
		constraints.gridy = 2;
		tfTelefone.setPreferredSize(new Dimension(100, 20));
		container.add(tfTelefone, constraints);
		// cbCargo
		cbCargo = new JComboBox(cargos);
		constraints.gridx = 1;
		constraints.gridy = 3;
		container.add(cbCargo, constraints);
		// btOk
		btAlterar = new JButton();
		btAlterar.setActionCommand("alterar");
		btAlterar.addActionListener(gBtn);
		// btOk.addActionListener();
		btAlterar.setText("Alterar!");
		constraints.gridx = 1;
		constraints.gridy = 4;
		container.add(btAlterar, constraints);
		// config JFrame
		setSize(550, 350);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	public void pegaMatricula(int matricula){
		mat = matricula;
	}
	public void preencheCampos(){
		tfNome.setText(ctrlFuncionario.encontraFuncionarioPorMatricula(mat).getNome());
		tfDataDeNascimento.setText(ctrlFuncionario.encontraFuncionarioPorMatricula(mat).getDataDeNascimento());
		tfTelefone.setText(ctrlFuncionario.encontraFuncionarioPorMatricula(mat).getTelefone());
	}
	public class GerenciadorBotoesAlterarFunc implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("alterar")){
				try {
					ctrlFuncionario.alteraTela(mat, tfNome.getText(), tfDataDeNascimento.getText(),
								tfTelefone.getText(), cbCargo.getSelectedIndex() + 1);
//					ctrlFuncionario.atualizaTabelas();
					dispose();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "DEU MERDA 2");
				}
			}
		}
		
	}
}
