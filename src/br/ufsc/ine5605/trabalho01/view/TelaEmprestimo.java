package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.control.ControleEmprestimo;
import br.ufsc.ine5605.trabalho01.model.Veiculo;

public class TelaEmprestimo extends JFrame{
	
	private String[] colunas = { "Placa", "Modelo", "Marca", "Ano", "Quilometragem", "Disponibilidade", "Funcionario" };
	private Claviculario ctrl;
	private GerenciadorBotoesFunc gBtn;
	private DefaultTableModel tableModel;
	private JTable table;
	private int matricula;

	public TelaEmprestimo(Claviculario owner) {
		super("Emprestimo");
		this.ctrl = owner;
		gBtn = new GerenciadorBotoesFunc();
	}
	
	public void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		tableModel = new DefaultTableModel(colunas, 0);
		updateData();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 699, 377);
		container.add(scrollPane);
		
		table = new JTable();
		table.setModel(tableModel);
		scrollPane.setViewportView(table);
		
		JButton btEntraMatricula = new JButton();
		btEntraMatricula.setActionCommand("Alugar");
		btEntraMatricula.addActionListener(gBtn);
		// btEntraMatricula.addActionListener();
		btEntraMatricula.setText("Alugar");
		btEntraMatricula.setPreferredSize(new Dimension(100, 50));
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(btEntraMatricula, constraints);

		
		// config JFrame
		setSize(900, 700);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public void updateData() {
		// TODO Auto-generated method stub
		tableModel.setNumRows(0);
		for (Veiculo v : ctrl.getCtrlVeiculo().getVeiculoDAO().getList()) {
			String placa = v.getPlaca();
			String modelo = v.getModelo();
			String marca = v.getMarca();
			int ano = v.getAno();
			int quilometragem = v.getQuilometragem();
			boolean disponibilidade = v.isDisponibilidade();
			Object[] row = {placa, modelo, marca, ano, quilometragem, disponibilidade};
			tableModel.addRow(row);
		}
	}
	
	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Alugar")) {
				String placa =  (String) table.getValueAt(table.getSelectedRow(), 0);
				try {
					if (!ctrl.getCtrlFuncionario().encontraFuncionarioPorMatricula(matricula).isBloqueado()) {
						ctrl.alugaVeiculo(matricula, placa);
						JOptionPane.showMessageDialog(null, "Ve�culo alugado com sucesso!");
						updateData();
						dispose();
					}
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		}

	}

	public void pegaMatricula(int matricula) {
		this.matricula = matricula;
	}
}

