package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.control.ControleVeiculo;
import br.ufsc.ine5605.trabalho01.model.Veiculo;

@SuppressWarnings("serial")
public class TelaControleVeiculo extends JFrame {

	private ControleVeiculo ctrl;
	private GerenciadorBotoesFunc gBtn;

	private String[] colunas = { "Placa", "Modelo", "Marca", "Ano", "Quilometragem", "Disponibilidade", "Funcionario" };
	private JButton btCadastraVeiculo;
	private JButton btAlteraVeiculo;
	private JButton btExcluiVeiculo;
	private JButton btVoltar;
	private DefaultTableModel tableModel;
	
	private JTable table;
	
	public TelaControleVeiculo(ControleVeiculo owner) {
		super("Controle de veiculo");
		this.ctrl = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	public void init() {
		Container container = getContentPane();
		tableModel = new DefaultTableModel(colunas, 0);
		updateData();
		
		container.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 699, 377);
		container.add(scrollPane);
		
		table = new JTable();
		table.setModel(tableModel);
		scrollPane.setViewportView(table);
		
		btCadastraVeiculo = new JButton("Cadastrar novo");
		btCadastraVeiculo.setActionCommand("CadastraVeiculo");
		btCadastraVeiculo.setBounds(732, 82, 150, 23);
		btCadastraVeiculo.addActionListener(gBtn);
		container.add(btCadastraVeiculo);
		
		btExcluiVeiculo = new JButton("Excluir");
		btExcluiVeiculo.setActionCommand("Excluir");
		btExcluiVeiculo.setBounds(732, 120, 150, 23);
		btExcluiVeiculo.addActionListener(gBtn);
		container.add(btExcluiVeiculo);
		
		btAlteraVeiculo = new JButton("Alterar");
		btAlteraVeiculo.setActionCommand("Alterar");
		btAlteraVeiculo.setBounds(732, 158, 150, 23);
		btAlteraVeiculo.addActionListener(gBtn);
		container.add(btAlteraVeiculo);
			
		btVoltar = new JButton("Voltar");
		btVoltar.setActionCommand("Voltar");
		btVoltar.setBounds(732, 196, 150, 23);
		btVoltar.addActionListener(gBtn);
		container.add(btVoltar);
		
		// config JFrame
		setSize(910, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}

	public void updateData() {
		tableModel.setNumRows(0);
		for (Veiculo v : ctrl.getVeiculoDAO().getList()) {
			String placa = v.getPlaca();
			String modelo = v.getModelo();
			String marca = v.getMarca();
			int ano = v.getAno();
			int quilometragem = v.getQuilometragem();
			boolean disponibilidade = v.isDisponibilidade();
			Object[] row = {placa, modelo, marca, ano, quilometragem, disponibilidade};
			tableModel.addRow(row);
		}
	}

	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String opcao = e.getActionCommand();
			switch(opcao) {
			case "Voltar":
				dispose();
				break;
			case "CadastraVeiculo":
				ctrl.abreCadastroVeiculo();
				dispose();
				break;
			case "Excluir":
				String placa = "";
				try {
					placa = (String) table.getValueAt(table.getSelectedRow(), 0);
					try {
						ctrl.removerVeiculo(placa);
						try {
							table.remove(table.getSelectedRow());
						} catch (Exception e1) {
						}
						JOptionPane.showMessageDialog(null, "Veiculo excluido.");
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, e2.getMessage());
					}
				} catch (Exception e3) {
					JOptionPane.showMessageDialog(null, "Selecione um veiculo para excluir");
				}
				
				
				
				break;
			default:
			case "Alterar":
				String placa2 = JOptionPane.showInputDialog("Digite a placa do veiculo para alterar:");
				dispose();
				ctrl.abreTelaAlteraVeiculo(placa2);
				break;
			}
		}

	}
}
