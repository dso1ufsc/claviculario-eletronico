package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufsc.ine5605.trabalho01.control.Claviculario;

public class TelaDevolveVeiculo extends JFrame {

	private GerenciadorBotoesFunc gBtn;
	private Claviculario claviculario;

	private JLabel lbMatricula;
	private JLabel lbPlaca;
	private JTextField tfPlaca;
	private JTextField tfMatricula;
	private JLabel lbQuilometragem;
	private JTextField tfQuilometragem;
	private JButton btEntraMatricula;
	
	private Dimension tamanhoBotao = new Dimension(150, 30);

	public TelaDevolveVeiculo(Claviculario owner) {
		super("Devolucao de veiculos");
		this.claviculario = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	private void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		
		lbMatricula = new JLabel();
		lbMatricula.setText("Matricula: ");
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(lbMatricula, constraints);
		tfMatricula = new JTextField();
		constraints.gridx = 1;
		constraints.gridy = 1;
		tfMatricula.setPreferredSize(new Dimension(100, 20));
		container.add(tfMatricula, constraints);
		
		lbPlaca = new JLabel();
		lbPlaca.setText("Placa: ");
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(lbPlaca, constraints);
		tfPlaca = new JTextField();
		constraints.gridx = 1;
		constraints.gridy = 2;
		tfPlaca.setPreferredSize(new Dimension(100, 20));
		container.add(tfPlaca, constraints);
		
		lbQuilometragem = new JLabel();
		lbQuilometragem.setText("Nova quilometragem: ");
		constraints.gridx = 0;
		constraints.gridy = 3;
		container.add(lbQuilometragem, constraints);
		tfQuilometragem = new JTextField();
		constraints.gridx = 1;
		constraints.gridy = 3;
		tfQuilometragem.setPreferredSize(new Dimension(100, 20));
		container.add(tfQuilometragem, constraints);
		
		btEntraMatricula = new JButton();
		btEntraMatricula.setActionCommand("Devolver");
		btEntraMatricula.addActionListener(gBtn);
		// btEntraMatricula.addActionListener();
		btEntraMatricula.setText("Devolve");
		btEntraMatricula.setPreferredSize(tamanhoBotao);
		constraints.gridx = 1;
		constraints.gridy = 4;
		container.add(btEntraMatricula, constraints);
		

		// config JFrame
		setSize(400, 200);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Devolver")) {
				String placa = tfPlaca.getText();
				Integer matricula = 0;
				Integer quilometragem = 0;
				try {
					matricula = Integer.parseInt(tfMatricula.getText());
					quilometragem = Integer.parseInt(tfQuilometragem.getText());
					if (claviculario.getCtrlEmprestimo().verificaEmprestimoMatriculaPlaca(matricula, placa)) {
						try {
							claviculario.devolveVeiculo(matricula, placa, quilometragem);
							JOptionPane.showMessageDialog(null, "Veiculo devolvido!");
							dispose();
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
						}
					} else {
						JOptionPane.showMessageDialog(null, "Voc� n�o alugou este ve�culo.");
					}
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Digite uma matricula v�lida.");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
			apagarCampos();
		}
		public void apagarCampos() {
			tfMatricula.setText("");
			tfPlaca.setText("");
			tfQuilometragem.setText("");
		}

	}
}
