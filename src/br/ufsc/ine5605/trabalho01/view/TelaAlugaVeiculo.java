package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufsc.ine5605.trabalho01.control.Claviculario;

public class TelaAlugaVeiculo extends JFrame {

	private GerenciadorBotoesFunc gBtn;
	private Claviculario claviculario;

	private JLabel lbMatricula;
	private JTextField tfMatricula;
	private JButton btEntraMatricula;
	
	private Dimension tamanhoBotao = new Dimension(150, 30);

	public TelaAlugaVeiculo(Claviculario owner) {
		super("Aluguel de veiculos");
		this.claviculario = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	private void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		
		lbMatricula = new JLabel();
		lbMatricula.setText("Matricula: ");
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(lbMatricula, constraints);
		tfMatricula = new JTextField();
		tfMatricula.setText("");
		constraints.gridx = 1;
		constraints.gridy = 1;
		tfMatricula.setPreferredSize(new Dimension(100, 20));
		container.add(tfMatricula, constraints);
		
		btEntraMatricula = new JButton();
		btEntraMatricula.setActionCommand("Entra");
		btEntraMatricula.addActionListener(gBtn);
		// btEntraMatricula.addActionListener();
		btEntraMatricula.setText("Entra");
		btEntraMatricula.setPreferredSize(tamanhoBotao);
		constraints.gridx = 1;
		constraints.gridy = 2;
		container.add(btEntraMatricula, constraints);
		

		// config JFrame
		setSize(400, 200);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Entra")) {
				String sMatricula = tfMatricula.getText();
				int matricula = 0;
				try {
					matricula = Integer.parseInt(sMatricula);
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Digite uma matricula valida.");
				}
				try {
					if (!claviculario.getCtrlEmprestimo().verificaTemEmprestimoFuncionario(matricula) && !claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(matricula).isBloqueado()) {
						try {
							apagarCampos();
							claviculario.abreTelaEmprestimo(matricula);
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
						}
					} else {
						JOptionPane.showMessageDialog(null, "Voc� j� tem ve�culo alugado. Devolva para alugar novamente.");
					}
				} catch (HeadlessException e1) {
					// TODO Auto-generated catch block
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
				
			}
		}
		
		public void apagarCampos() {
			tfMatricula.setText("");
		}

	}
}
