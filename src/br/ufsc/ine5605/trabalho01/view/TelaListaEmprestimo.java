package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.model.Emprestimo;

@SuppressWarnings("serial")
public class TelaListaEmprestimo extends JFrame {

	private Claviculario ctrl;
	private GerenciadorBotoesFunc gBtn;

	private String[] colunas = { "C�digo", "Funcionario", "Veiculo" };
	private JButton btVoltar;
	private DefaultTableModel tableModel;
	
	private JTable table;
	
	public TelaListaEmprestimo(Claviculario owner) {
		super("Lista de emprestimos");
		this.ctrl = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	private void init() {
		Container container = getContentPane();
		tableModel = new DefaultTableModel(colunas, 0);
		updateData();
		
		container.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 699, 377);
		container.add(scrollPane);
		
		table = new JTable();
		table.setModel(tableModel);
		scrollPane.setViewportView(table);
		
		btVoltar = new JButton("Voltar");
		btVoltar.setActionCommand("Voltar");
		btVoltar.setBounds(732, 289, 150, 23);
		btVoltar.addActionListener(gBtn);
		container.add(btVoltar);
		
		// config JFrame
		setSize(910, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}

	public void updateData() {
		// TODO Auto-generated method stub
		tableModel.setNumRows(0);
		for (Emprestimo e : ctrl.getCtrlEmprestimo().getEmprestimoDAO().getList()) {
			int codigo = e.getCodigo();
			String nome = e.getFuncionario().getNome();
			String veiculo = e.getVeiculo().getPlaca();
			Object[] row = {codigo, nome, veiculo};
			tableModel.addRow(row);
		}
	}

	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String opcao = e.getActionCommand();
			switch(opcao) {
			case "Voltar":
				dispose();
				break;
			default:
				break;
			}
		}

	}
}
