package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;
import br.ufsc.ine5605.trabalho01.model.Veiculo;

public class TelaAddVeiculosFunc extends JFrame {
	private ControleFuncionario ctrlFuncionario;
	private GerenciadorBotoesTelaAddVFunc gBtn;

	private DefaultTableModel tabelaV;
	private DefaultTableModel tabelaFV;
	private JTable tabelaVeiculos;
	private JTable tabelaVeiculosFuncionario;
	private int mat;
	int numLinha = 0;
	private JButton btAdicionar;
	private JButton btVoltar;
	// private JButton btExcluir;
	private String[] colunas = { "Placa", "Modelo", "Marca", "Ano", "Quilometragem", "Disponibilidade" };

	public TelaAddVeiculosFunc(ControleFuncionario owner) {
		super("Adicionar veiculos ao funcionario");
		this.ctrlFuncionario = owner;
		this.mat = 0;
		this.gBtn = new GerenciadorBotoesTelaAddVFunc();
		this.tabelaV = new DefaultTableModel(colunas, 0);
		this.tabelaFV = new DefaultTableModel(colunas, 0);
	}

	public void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// tabela
		container.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane.setBounds(10, 12, 600, 300);
		scrollPane2.setBounds(640, 12, 600, 300);
		container.add(scrollPane);
		container.add(scrollPane2);
		// tabela1
		tabelaVeiculos = new JTable();
		tabelaVeiculos.setModel(tabelaV);
		scrollPane.setViewportView(tabelaVeiculos);
		// tabela2
		tabelaVeiculosFuncionario = new JTable();
		tabelaVeiculosFuncionario.setModel(tabelaFV);
		scrollPane2.setViewportView(tabelaVeiculosFuncionario);
		// btAdicionar
		btAdicionar = new JButton();
		btAdicionar.setText("Adicionar");
		btAdicionar.setActionCommand("Adicionar");
		btAdicionar.setBounds(220, 320, 150, 50);
		btAdicionar.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 9;
		container.add(btAdicionar, constraints);
		// btVoltar
		btVoltar = new JButton();
		btVoltar.setText("Voltar");
		btVoltar.setActionCommand("voltar");
		btVoltar.addActionListener(gBtn);
		btVoltar.setBounds(390, 320, 150, 50);
		container.add(btVoltar);
		// // btExcluir
		// btExcluir = new JButton();
		// btExcluir.setText("Excluir");
		// btExcluir.setActionCommand("excluir");
		// btExcluir.setBounds(370, 320, 150, 50);
		// btExcluir.addActionListener(gBtn);
		// constraints.gridx = 0;
		// constraints.gridy = 9;
		// container.add(btExcluir, constraints);
		// config JFrame
		setSize(1280, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	public void pegaMatricula(int matricula){
		mat = matricula;
	}
	public void updateDataFuncVeiculos() {
		tabelaFV.setNumRows(0);
		for (Veiculo v : ctrlFuncionario.getVeiculosFuncPorMatricula(mat)) {
			String placa = v.getPlaca();
			String modelo = v.getModelo();
			String marca = v.getMarca();
			int ano = v.getAno();
			int quilometragem = v.getQuilometragem();
			boolean disponibilidade = v.isDisponibilidade();

			Object[] row = { placa, modelo, marca, ano, quilometragem, disponibilidade };
			tabelaFV.addRow(row);
		}
	}

	public void updateDataVeiculos() {
		tabelaV.setRowCount(0);
		for (Veiculo v : Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().getList()) {
			String placa = v.getPlaca();
			String modelo = v.getModelo();
			String marca = v.getMarca();
			int ano = v.getAno();
			int quilometragem = v.getQuilometragem();
			boolean disponibilidade = v.isDisponibilidade();

			Object[] row = { placa, modelo, marca, ano, quilometragem, disponibilidade };
			tabelaV.addRow(row);
		}
	}

	public class GerenciadorBotoesTelaAddVFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Adicionar")) {
//				numLinha = tabelaVeiculos.getSelectedRow();
//				System.out.println("NumLinha = " +numLinha);
				String placa = "";
				placa = JOptionPane.showInputDialog("Digite a placa do veiculo para adicionar");
//				try {
//					placa = (String)tabelaVeiculos.getValueAt(numLinha, 0); 
//				} catch (Exception e2) {
//					JOptionPane.showMessageDialog(null, "Selecione um veiculo para realizar a operacao");
//				}
				try {
					
					ctrlFuncionario.adicionaVeiculoAoFuncionario(placa, mat);
					updateDataFuncVeiculos();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}

			} else if (e.getActionCommand().equals("voltar")) {
//				ctrlFuncionario.atualizaTabelaVeiculosFunc();
				dispose();
			}
		}

	}
}
