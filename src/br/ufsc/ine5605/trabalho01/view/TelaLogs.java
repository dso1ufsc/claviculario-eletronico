package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.ControleLogs;
import br.ufsc.ine5605.trabalho01.model.Logs;

public class TelaLogs extends JFrame {

	private GerenciadorBotoesLogs gBtn;
	private ControleLogs ctrlLogs;
	// componentes
	private String[] colunas = { "Codigo", "Data", "Status", "Funcionario", "Veiculo" };
	private JButton btVoltar;
	private JButton btMotivo;
	private JButton btMatricula;
	private JButton btPlaca;
	private JButton btTodos;
	private JTextField tfCampo;
	private DefaultTableModel tabelaL;
	private JTable tabelaLogs;

	public TelaLogs(ControleLogs owner) {
		super("Tela Logs");
		this.ctrlLogs = owner;
		this.gBtn = new GerenciadorBotoesLogs();
		this.tabelaL = new DefaultTableModel(colunas, 0);
		init();
	}

	public void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		container.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 12, 600, 300);
		container.add(scrollPane);

		tabelaLogs = new JTable();
		tabelaLogs.setModel(tabelaL);
		scrollPane.setViewportView(tabelaLogs);
		// btMotivo
		btMotivo = new JButton();
		btMotivo.setText("Motivo");
		btMotivo.setActionCommand("motivo");
		btMotivo.addActionListener(gBtn);
		btMotivo.setBounds(10, 370, 150, 50);
		container.add(btMotivo);
		// btMatricula
		btMatricula = new JButton();
		btMatricula.setText("Pesquisar por matricula");
		btMatricula.setActionCommand("matricula");
		btMatricula.addActionListener(gBtn);
		btMatricula.setBounds(170, 370, 150, 50);
		container.add(btMatricula);
		// btPlaca
		btPlaca = new JButton();
		btPlaca.setText("Pesquisar por placa");
		btPlaca.setActionCommand("placa");
		btPlaca.addActionListener(gBtn);
		btPlaca.setBounds(340, 370, 150, 50);
		container.add(btPlaca);
		// btVoltar
		btVoltar = new JButton();
		btVoltar.setText("Voltar");
		btVoltar.setActionCommand("voltar");
		btVoltar.addActionListener(gBtn);
		btVoltar.setBounds(500, 370, 150, 50);
		container.add(btVoltar);
		// btTodos
		btTodos = new JButton();
		btTodos.setText("Listar todos");
		btTodos.setActionCommand("todos");
		btTodos.addActionListener(gBtn);
		btTodos.setBounds(660, 370, 150, 50);
		container.add(btTodos);
		// tfCampo
		// tfCampo = new JTextField();
		// tfCampo.setText("");
		// tfCampo.setBounds(250, 320, 150, 30);
		// container.add(tfCampo);
		// config JFrame
		setSize(910, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void updateData(ArrayList<Logs> logs) {
		tabelaL.setNumRows(0);
		for (Logs log : logs) {
			String codigo = Integer.toString(log.getCodigo());
			String data = log.getData();
			String status = log.getStatus();
			String funcionario = log.getFuncionario().getNome();
			String veiculo = log.getVeiculo().getPlaca();
			Object[] row = { codigo, data, status, funcionario, veiculo };
			tabelaL.addRow(row);
		}
	}

	public class GerenciadorBotoesLogs implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("motivo")) {
				String campo = "";
				campo = JOptionPane.showInputDialog("Digite um motivo");
				if (campo == null || campo.equals("")) {
					campo = "";
				}
				if(!campo.isEmpty()){
					updateData(ctrlLogs.encontraLogPorMotivo(campo));
				}else {
					JOptionPane.showMessageDialog(null, "Digite um motivo");
				}
				// if(tfCampo.getText().equals("")||tfCampo == null){
				// JOptionPane.showMessageDialog(null, "Digite um motivo");
				// }else{
				// updateData(ctrlLogs.encontraLogPorMotivo(tfCampo.getText()));
				// }
			} else if (e.getActionCommand().equals("placa")) {
				String campo = "";
				campo = JOptionPane.showInputDialog("Digite uma placa");
				if (campo == null || campo.equals("")) {
					campo = "";
				}
				if(!campo.isEmpty()){
					updateData(ctrlLogs.encontraLogPorPlacaVeiculo(campo));
				}else {
					JOptionPane.showMessageDialog(null, "Digite uma placa valida");
				}
				
				// if(tfCampo.getText().equals("")||tfCampo == null){
				// JOptionPane.showMessageDialog(null, "Digite uma placa");
				// }else{
				// updateData(ctrlLogs.encontraLogPorPlacaVeiculo(tfCampo.getText()));
				// }
			} else if (e.getActionCommand().equals("matricula")) {
				String sMatricula = "";
				sMatricula = JOptionPane.showInputDialog("Digite a matricula do funcionario");
				if (sMatricula == null || sMatricula.equals("")) {
					sMatricula = "0";
				}
				int matricula = Integer.parseInt(sMatricula);
				if (matricula != 0) {
					updateData(ctrlLogs.encontraLogPorMatriculaFuncionario(matricula));
				} else {
					JOptionPane.showMessageDialog(null, "Digite uma matricula valida");
				}
				// if(tfCampo.getText().matches("\\d+")){
				// int mats = Integer.parseInt(tfCampo.getText());
				// updateData(ctrlLogs.encontraLogPorMatriculaFuncionario(mats));
				// }else{
				// JOptionPane.showMessageDialog(null, "Digite uma matricula
				// somente com numeros");
				// }

			} else if (e.getActionCommand().equals("voltar")) {
				setVisible(false);
				init();
				dispose();
			} else if (e.getActionCommand().equals("todos")) {
				updateData(ctrlLogs.getLogs());
			}
		}

	}
}
