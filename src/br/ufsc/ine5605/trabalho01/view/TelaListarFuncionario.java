package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;
import br.ufsc.ine5605.trabalho01.model.Cargo;
import br.ufsc.ine5605.trabalho01.model.Funcionario;

public class TelaListarFuncionario extends JFrame {
	private GerenciadorBotoesListarFunc gBtn;
	private ControleFuncionario ctrlFuncionario;
	// Componentes
	private String[] colunas = { "Matricula", "Nome", "Cargo", "Data de nascimento", "Telefone", "Tentativa de acesso",
			"Carro alugado" };
	private JButton btVeiculos;
	private JButton btExcluir;
	private JButton btAlterar;
	private JButton btVoltar;
	private DefaultTableModel tabelaF;

	int numLinha = 0;

	private JTable tabelaFuncionarios;
	private JScrollPane scroll;

	private Dimension tamanhoBotao = new Dimension(150, 50);

	public TelaListarFuncionario(ControleFuncionario owner) {
		super("Listar funcionarios");
		this.ctrlFuncionario = owner;
		this.gBtn = new GerenciadorBotoesListarFunc();
		this.tabelaF = new DefaultTableModel(colunas, 0);
	}

	public void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// TABELA
		updateData();
		container.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 12, 600, 300);
		container.add(scrollPane);

		tabelaFuncionarios = new JTable();
		tabelaFuncionarios.setModel(tabelaF);
		scrollPane.setViewportView(tabelaFuncionarios);

		// btVeiculos
		btVeiculos = new JButton();
		btVeiculos.setText("Veiculos");
		btVeiculos.setActionCommand("veiculos");
		btVeiculos.addActionListener(gBtn);
		btVeiculos.setBounds(615, 12, 150, 50);
		constraints.gridx = 0;
		constraints.gridy = 10;
		container.add(btVeiculos, constraints);
		// btAlterar
		btAlterar = new JButton();
		btAlterar.setText("Alterar");
		btAlterar.setActionCommand("alterar");
		btAlterar.addActionListener(gBtn);
		btAlterar.setBounds(615, 68, 150, 50);
		constraints.gridx = 0;
		constraints.gridy = 5;
		container.add(btAlterar, constraints);
		// btExcluir
		btExcluir = new JButton();
		btExcluir.setText("Excluir");
		btExcluir.setActionCommand("excluir");
		btExcluir.addActionListener(gBtn);
		btExcluir.setBounds(615, 124, 150, 50);
		constraints.gridx = 0;
		constraints.gridy = 7;
		container.add(btExcluir, constraints);
		// btVoltar
		btVoltar = new JButton();
		btVoltar.setText("Voltar");
		btVoltar.setActionCommand("voltar");
		btVoltar.setBounds(615, 180, 150, 50);
		btVoltar.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 9;
		container.add(btVoltar, constraints);

		// config JFrame
		setSize(910, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void updateData() {
		tabelaF.setNumRows(0);
		for (Funcionario f : this.ctrlFuncionario.getFuncionarioDAO().getList()) {
			String matricula = Integer.toString(f.getMatricula());
			String nome = f.getNome();
			Cargo cargo = f.getCargo();
			String dataNascimento = f.getDataDeNascimento();
			String telefone = f.getTelefone();
			String tentativaAcesso = Integer.toString(f.getTentativaDeAcesso());
			String carroAlugado = "Sem carro";
			if (ctrlFuncionario.carroAlugado(f.getMatricula()) != null) {
				carroAlugado = ctrlFuncionario.carroAlugado(f.getMatricula()).getPlaca();
			}
			Object[] row = { matricula, nome, cargo, dataNascimento, telefone, tentativaAcesso, carroAlugado };
			tabelaF.addRow(row);
		}
	}

	private class GerenciadorBotoesListarFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getActionCommand().equals("voltar")) {
				dispose();
			} else if (e.getActionCommand().equals("excluir")) {
				String sMatricula = "";
				sMatricula = JOptionPane.showInputDialog("Digite a matricula do funcionario para excluir");
				if (sMatricula == null || sMatricula.equals("")) {
					sMatricula = "0";
				}
				int matricula = Integer.parseInt(sMatricula);
				// try {
				// matricula = Integer.parseInt((String)
				// tabelaFuncionarios.getValueAt(tabelaFuncionarios.getSelectedRow(),
				// 0));
				// } catch (IndexOutOfBoundsException e2) {
				// JOptionPane.showMessageDialog(null, "Selecione um funcionario
				// para realizar a operacao");
				// }
				if (matricula != 0) {
					try {
						ctrlFuncionario.removeFuncionarioPorMatricula(matricula);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage());
					}
				} else {
					JOptionPane.showMessageDialog(null, "Digite uma matricula valida");
				}

			} else if (e.getActionCommand().equals("veiculos")) {
				// tratar exce��o
				String sMatricula = "";
				sMatricula = JOptionPane
						.showInputDialog("Digite a matricula do funcionario ao qual voc� quer ver os veiculos");
				if (sMatricula == null || sMatricula.equals("")) {
					sMatricula = "0";
				}
				int matricula = Integer.parseInt(sMatricula);
				// try {
				// matricula = Integer.parseInt((String)
				// tabelaFuncionarios.getValueAt(tabelaFuncionarios.getSelectedRow(),0));
				// } catch (ArrayIndexOutOfBoundsException e1) {
				// JOptionPane.showMessageDialog(null, "Selecione um funcionario
				// para realizar a opera��o.");
				// System.out.println("getSelectedRow() = "
				// +tabelaFuncionarios.getSelectedRow());
				// }
				// System.out.println("tela veiculos "+matricula);
				if (matricula != 0) {
					ctrlFuncionario.telaVeiculosFunc(matricula);
				} else {
					JOptionPane.showMessageDialog(null, "Digite uma matricula valida");
				}

			} else if (e.getActionCommand().equals("alterar")) {
				String sMatricula = "";
				sMatricula = JOptionPane.showInputDialog("Digite a matricula do funcionario para alterar");
				if (sMatricula == null || sMatricula.equals("")) {
					sMatricula = "0";
				}
				int matricula = Integer.parseInt(sMatricula);
				// try {
				// matricula = Integer.parseInt((String)
				// tabelaFuncionarios.getValueAt(tabelaFuncionarios.getSelectedRow(),
				// 0));
				// } catch (ArrayIndexOutOfBoundsException e1) {
				// JOptionPane.showMessageDialog(null, "Selecione um funcionario
				// para realizar a operacao.");
				// }
				if (matricula != 0) {
					try {
						ctrlFuncionario.telaAlterarFunc(matricula);
					} catch (NullPointerException e1) {
						JOptionPane.showMessageDialog(null, "A matricula n�o existe. Matricula -> " + matricula);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Digite uma matricula valida");
				}

			}
		}

	}
}
