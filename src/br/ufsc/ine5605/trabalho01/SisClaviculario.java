package br.ufsc.ine5605.trabalho01;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.model.Funcionario;
import br.ufsc.ine5605.trabalho01.model.Veiculo;

public class SisClaviculario {

	public static void main(String[] args) {
		Claviculario claviculario = Claviculario.getInstance();
		claviculario.inicia();
		
	}
}
