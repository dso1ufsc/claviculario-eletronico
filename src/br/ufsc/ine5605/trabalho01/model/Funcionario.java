package br.ufsc.ine5605.trabalho01.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Funcionario extends Pessoa implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int matricula;
	private Cargo cargo;
	private ArrayList<Veiculo> veiculosPermitidos;
	private int tentativaDeAcesso;
	private boolean isBloqueado;
	
	public Funcionario(int matricula, String nome, String dataDeNascimento, String telefone) {
		super(nome, dataDeNascimento, telefone);
		this.matricula = matricula;
		this.veiculosPermitidos = new ArrayList<>();
		this.tentativaDeAcesso = 0;
		this.isBloqueado = false;
	}
	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public ArrayList<Veiculo> getVeiculosPermitidos() {
		return veiculosPermitidos;
	}
	
	public int getTentativaDeAcesso() {
		return tentativaDeAcesso;
	}
	
	public void setTentativaDeAcesso(int tentativaDeAcesso) throws Exception {
		if (this.tentativaDeAcesso == 2) {
			this.isBloqueado = true;
			throw new Exception("Funcionario bloqueado.");
		}
		this.tentativaDeAcesso = tentativaDeAcesso;
	}
	
	public boolean isBloqueado() throws Exception{
        if (isBloqueado) {
            throw new Exception("Funcionario esta bloqueado.");
        }
        return isBloqueado;
    }
	
	public void setBloqueado(boolean isBloqueado) {
		this.isBloqueado = isBloqueado;
	}
	
	public void setVeiculosPermitidos(ArrayList<Veiculo> veiculosPermitidos) {
		this.veiculosPermitidos = veiculosPermitidos;
	}
	
	
	
}
