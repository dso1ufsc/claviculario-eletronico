package br.ufsc.ine5605.trabalho01.model;

import java.io.Serializable;

public class Emprestimo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int codigo;
	private Funcionario funcionario;
	private Veiculo veiculo;
	
	public Emprestimo(Funcionario funcionario, Veiculo veiculo) {
		this.funcionario = funcionario;
		this.veiculo = veiculo;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	

}
