package br.ufsc.ine5605.trabalho01.model;

public interface ICadastraListaExclui {
	
	public void cadastra(Object o) throws Exception;
	public String lista() throws Exception;
	public void exclui(Object o) throws Exception;

}
