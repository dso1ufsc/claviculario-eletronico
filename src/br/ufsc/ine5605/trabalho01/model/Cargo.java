package br.ufsc.ine5605.trabalho01.model;

public enum Cargo {

	DIRETORIA(),
	SERVICOSGERAIS(),
	RH(),
	INFORMATICA();

}
