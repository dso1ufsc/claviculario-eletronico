package br.ufsc.ine5605.trabalho01.model;

import java.io.Serializable;

public abstract class Pessoa implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nome;
	private String dataDeNascimento;
	private String telefone;
	
	public Pessoa(String nome, String dataDeNascimento, String telefone) {
		this.nome = nome;
		this.dataDeNascimento = dataDeNascimento;
		this.telefone = telefone;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDataDeNascimento() {
		return dataDeNascimento;
	}
	public void setDataDeNascimento(String dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	

}
