package br.ufsc.ine5605.trabalho01.persistencia;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.ufsc.ine5605.trabalho01.model.Funcionario;

public class FuncionarioDAO {
	private HashMap<Integer, Funcionario> cacheFuncionarios;
	private final String filename = "funcionario.cla";

	public FuncionarioDAO() {
		cacheFuncionarios = new HashMap<>();
		load();
	}
	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);

			this.cacheFuncionarios = (HashMap<Integer, Funcionario>) oit.readObject();
			
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public void put(Funcionario funcionario) throws Exception {
		if (funcionario != null) {
			cacheFuncionarios.put(funcionario.getMatricula(), funcionario);
			persist();
		}
	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheFuncionarios);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Funcionario get(Integer matricula) {
		return cacheFuncionarios.get(matricula);
	}

	public void remove(Integer matricula) {
		cacheFuncionarios.remove(matricula);
		persist();
	}

	public Collection<Funcionario> getList() {
		return cacheFuncionarios.values();
	}

}
