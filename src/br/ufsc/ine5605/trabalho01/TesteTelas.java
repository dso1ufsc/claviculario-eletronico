package br.ufsc.ine5605.trabalho01;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;
import br.ufsc.ine5605.trabalho01.control.ControleVeiculo;
import br.ufsc.ine5605.trabalho01.model.Veiculo;
import br.ufsc.ine5605.trabalho01.persistencia.VeiculoDAO;
import br.ufsc.ine5605.trabalho01.view.TelaControleVeiculo;

public class TesteTelas {

	public static void main(String[] args) {
		Claviculario claviculario = Claviculario.getInstance();
		
		Veiculo v = new Veiculo("mmm9990", "F430", "Ferrari", 2008, 0);
		//Funcionario f = new Funcionario(11, "Gabriel", "01/07/97", "33339999");
		Veiculo v2 = new Veiculo("mmm9991", "Gol", "Volkswagem", 1999, 78900);
		//Funcionario f2 = new Funcionario(22, "Jonas", "01/07/97", "33339999");
		Veiculo v3 = new Veiculo("mmm9992", "Corsa", "Chevrolet", 2002, 67534);
		//Funcionario f3 = new Funcionario(33, "Coisa", "01/07/97", "33339999");
		Veiculo v4 = new Veiculo("mmm9993", "Chevette", "Chevrolet", 1986, 0);
		//Funcionario f4 = new Funcionario(44, "Deregue", "01/07/97", "33339999");
		Veiculo v5 = new Veiculo("mmm9994", "Gol", "Volkswagem", 2007, 9800);
		//Funcionario f5 = new Funcionario(25, "BillyTheJhonson", "01/07/97", "33339999");
		Veiculo v6 = new Veiculo("mmm9995", "HB20", "Hyundai", 1999, 0);
		//Funcionario f6 = new Funcionario(36, "Jaquet�ide", "01/07/97", "33339999");
		try {
			claviculario.getCtrlVeiculo().cadastra(v);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		try {
			claviculario.getCtrlFuncionario().cadastra(f);
			claviculario.getCtrlFuncionario().cadastraCargo(2, f);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		try {
			claviculario.getCtrlVeiculo().cadastra(v2);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f.getMatricula()).getVeiculosPermitidos().add(v2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f.getMatricula()).getVeiculosPermitidos().add(v3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			claviculario.getCtrlFuncionario().cadastra(f2);
			claviculario.getCtrlFuncionario().cadastraCargo(3, f2);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		try {
			claviculario.getCtrlVeiculo().cadastra(v3);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		try {
			claviculario.getCtrlFuncionario().cadastra(f3);
			claviculario.getCtrlFuncionario().cadastraCargo(4, f);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		*/
		try {
			claviculario.getCtrlVeiculo().cadastra(v4);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		try {
			claviculario.getCtrlFuncionario().cadastra(f4);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		try {
			claviculario.getCtrlVeiculo().cadastra(v5);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		try {
			claviculario.getCtrlFuncionario().cadastra(f5);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f2.getMatricula()).getVeiculosPermitidos().add(v4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f2.getMatricula()).getVeiculosPermitidos().add(v5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		try {
			claviculario.getCtrlVeiculo().cadastra(v6);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		try {
			claviculario.getCtrlFuncionario().cadastra(f6);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f3.getMatricula()).getVeiculosPermitidos().add(v2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f4.getMatricula()).getVeiculosPermitidos().add(v3);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f5.getMatricula()).getVeiculosPermitidos().add(v4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f6.getMatricula()).getVeiculosPermitidos().add(v5);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f6.getMatricula()).getVeiculosPermitidos().add(v2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			claviculario.getCtrlFuncionario().encontraFuncionarioPorMatricula(f6.getMatricula()).getVeiculosPermitidos().add(v3);
		} catch (Exception e6) {
			// TODO Auto-generated catch block
			e6.printStackTrace();
		}
		
		try {
			//claviculario.getCtrlFuncionario().cadastraCargo(1, f);
		} catch (Exception e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
		try {
			//claviculario.getCtrlFuncionario().cadastraCargo(2, f2);
		} catch (Exception e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		try {
			//claviculario.getCtrlFuncionario().cadastraCargo(3, f3);
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			//claviculario.getCtrlFuncionario().cadastraCargo(4, f4);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			//claviculario.getCtrlFuncionario().cadastraCargo(2, f5);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			//claviculario.getCtrlFuncionario().cadastraCargo(3, f6);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*try {
			claviculario.alugaVeiculo(16200901, "mmm9999");
			claviculario.alugaVeiculo(16200902, "mmm9993");
			claviculario.devolveVeiculo(16200901, "mmm9993");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			claviculario.alugaVeiculo(16200901, "mmm9992");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			claviculario.devolveVeiculo(16200902, "mmm9999");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}*/
//		ControleFuncionario ctrl = Claviculario.getInstance().getCtrlFuncionario();
//		ctrl.inicia();
		
//		ControleVeiculo ctrlVeiculo = Claviculario.getInstance().getCtrlVeiculo();
//		ctrlVeiculo.inicia();
//		System.out.println(ctrlVeiculo.getVeiculoDAO().getList().size());
//		for (Veiculo v : ctrlVeiculo.getVeiculoDAO().getList()) {
//			System.out.println("Placa: " +v.getPlaca());
//		}
		
		claviculario.inicia();
	}

}
